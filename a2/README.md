# LIS3781
## Mary Meberg

### **Assignment 2 Requirements:**

*Four Parts*

1. Tables and insert statements. 
2. Include indexes and foreign key SQL statements (see below). 
3. Include *your* query result sets, including grant statements. 
4. The following tables should be created and populated with at least 5 records both locally and to the CCI server. 


#### Assignment Screenshots:

#### SQL Code - Creation of mmm18m.company and mmm18m.customer
|*SQL Code Creating + Populating Tables*|*Population of Tables*|
|:-------------:|:-------------:|
|![IDLE Tip Calc.](img/table_creation_sql_code.png)| ![IDLE Tip Calc.](img/populated_tables.png)|

|*User 1 Login + Grants*|*User 2 Login + Grants*|
|:-------------:|:-------------:|
|![IDLE Tip Calc.](img/user1_login_and_grants.png)| ![IDLE Tip Calc.](img/user2_login_and_grants.png)|


#### User creation and granting privileges
|*Logging in as Admin and Viewing Grants*|*Creating Users and Giving Grants*|
|:-------------:|:-------------:|
|![Sq. Ft to Acres.](img/admin_login_and_grants.png) | ![Miles Per Gal](img/creation_of_users_and_grants.png) | 

#### Display current user (user2) and MySQL Version
|*Display current user and version*|
|:-------------:|
|![Sq. Ft to Acres.](img/user2_confirm_and_sql_version.png) |

#### List tables and display table structures
|*Admin Listing and Describing Tables*|
|:-------------:|
|![IDLE Tip Calc.](img/admin_list_tables_and_describe.png)|

#### Display data for both tables
|*Show 'company' as user2*|*Show 'customer' as user1*|
|:-------------:|:-------------:|
|![Sq. Ft to Acres.](img/user2_display_company_data.png) | ![Miles Per Gal](img/user1_display_customer_data.png) | 

#### Login as user1
|*Show SQL INSERT Statement and Results For 'company' and 'customer'*|
|:-------------:|
|![Sq. Ft to Acres.](img/user1_denied_rights.png) | 

#### Login as user2
|*Show SQL SELECT Statement For 'company + Show SQL DELETE Statement For 'customer'*|
|:-------------:|
|![Sq. Ft to Acres.](img/user2_denied_rights.png) | 

#### Login as admin and delete
![Sq. Ft to Acres.](img/admin_deletes_the_world.png) 

