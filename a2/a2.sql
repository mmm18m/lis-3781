drop database if exists mmm18m;
CREATE database IF NOT EXISTS mmm18m DEFAULT CHARACTER SET utf8 ;
USE mmm18m;
show tables;

-- -----------------------------------------------------
-- Table company
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS company (
  cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  cmp_type ENUM('C-Corp', 'S-Corp', 'Non-Profit-Corp', 'LLC', 'Partnership') NOT NULL,
  cmp_street VARCHAR(30) NOT NULL,
  cmp_city VARCHAR(30) NOT NULL,
  cmp_state CHAR(2) NOT NULL,
  cmp_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
  cmp_phone BIGINT UNSIGNED NOT NULL,
  cmp_ytd_sales DECIMAL(10,2) UNSIGNED NOT NULL,
  cmp_email VARCHAR(100) NULL,
  cmp_url VARCHAR(100) NULL,
  cmp_notes VARCHAR(255) NULL,
  PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB;

SHOW WARNINGS;
INSERT INTO company
VALUES
(null, 'C-Corp', '123 Angel Av', 'Las Vegas', 'NV', 00001, 1234567890, 10000.00, null, 'willyswood.com', null),
(null, 'S-Corp', '234 Boisenberry Rd', 'Boston', 'MA', 00002, 2344567896, 20000.00, null, 'bobsbinkies.com', null),
(null, 'Non-Profit-Corp', '345 Cattyfeld Way', 'Catalina', 'FL', 00003, 6556486355, 30000.00, null, 'carlascats.com', null),
(null, 'LLC', '456 Drayers Pass', 'Austin', 'TX', 00004, 6529639685, 40000.00,  null, 'jennasjelly.com', null),
(null, 'Partnership', '567 Engel Av', 'Atlanta', 'GA', 00005, 1237458963, 50000.00,  null, 'alexsalexas.com', null);

-- -----------------------------------------------------
-- Table customer
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS customer (
  cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  cmp_id INT UNSIGNED NOT NULL,
  cus_ssn BINARY(64) NOT NULL,
  cus_salt BINARY(64) NOT NULL,
  cus_type ENUM('Loyal', 'Discount', 'Impulse', 'Need-Based', 'Wandering') NOT NULL,
  cus_first VARCHAR(45) NOT NULL,
  cus_last VARCHAR(45) NOT NULL,
  cus_street VARCHAR(45) NOT NULL,
  cus_city VARCHAR(45) NOT NULL,
  cus_state CHAR(9) NOT NULL,
  cus_zip INT(9) ZEROFILL UNSIGNED NOT NULL,
  cus_phone BIGINT UNSIGNED NOT NULL,
  cus_email VARCHAR(100) NOT NULL,
  cus_balance DECIMAL(8,2) UNSIGNED NOT NULL,
  cus_tot_sales DECIMAL(8,2) UNSIGNED NOT NULL,
  cus_notes VARCHAR(255) NULL,
  PRIMARY KEY (cus_id),
  UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
  INDEX idx_cmp_id (cmp_id ASC),
  CONSTRAINT fk_cus_cmp
    FOREIGN KEY (cmp_id)
    REFERENCES company (cmp_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES 
(NULL, 1, unhex(SHA2(CONCAT(@salt, 000112222), 512)), @salt, 'Loyal', 'Amie', 'Anderson', 'Antelope Av', 'Austin', 'TX', 20012, 1112223333, 'amie@mail.com', 123.23, 123.23, NULL),
(NULL, 2, unhex(SHA2(CONCAT(@salt, 111223333), 512)), @salt, 'Discount', 'Barry', 'Bayoda', 'Bear Beltway', 'Boston', 'MA', 89563, 7418529635, 'barry@mail.com', 1123.23, 1123.23, NULL),
(NULL, 3, unhex(SHA2(CONCAT(@salt, 222334444), 512)), @salt, 'Impulse', 'Cathie', 'McCormick', 'Cat Rd.', 'Chicago', 'IL', 89453, 7894561289, 'cathie@mail.com', 11123.33, 11123.33, NULL),
(NULL, 4, unhex(SHA2(CONCAT(@salt, 333445555), 512)), @salt, 'Need-Based', 'Dillion', 'Dayers', 'Dog Ln.', 'Destin', 'FL', 05648, 9865432859, 'dillion@mail.com', 111123.33, 111123.33, NULL),
(NULL, 5, unhex(SHA2(CONCAT(@salt, 444556666), 512)), @salt, 'Wandering', 'Evelyn', 'Engelsfeld', 'Eagle Cir', 'Evansville', 'OH', 98538, 7538964299, 'evelyn@mail.com', 111123.33, 111123.33, NULL);


SHOW WARNINGS;

select * from company;
select * from customer;