# LIS3781
## Mary Meberg

### **Assignment 1 Requirements:**

*Four Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket Repository
    a. Assignment Repo
    b. bitbucketstationlocation Repo


#### **README.md file should include the following items:**

* Screenshot of A1 ERD
* Ex1 SQL Solution
* git commands with short description

#### Git commands w/short descriptions:

1. git init: Creates a new Git repository.
2. git status: Displays the state of the working directory and the staging area.
3. git add: Adds a change in the working directory to the staging area
4. git commit: Saves your changes to the local repository.
5. git push: Uploads local repository content to a remote repository.
6. git pull: Fetches and downloads content from a remote repository updates the local repository to match.
7. git branch: Lists all the branches in your repo, and tells you what branch you're currently in.

#### Assignment Screenshots:

#### Screenshot of A1 ERD
![IDLE Tip Calc.](img/erd.png)

#### Screenshot of A1 Ex 1 and 2
|*EX 1*|*EX 2*|
|:-------------:|:-------------:|
|![Sq. Ft to Acres.](img/q1.png) | ![Miles Per Gal](img/q2.png) | 

#### Screenshot of A1 Ex 3 and 4
|*EX 3*|*EX 4*|
|:-------------:|:-------------:|
|![Sq. Ft to Acres.](img/q3.png) | ![Miles Per Gal](img/q4.png) |

#### Screenshot of A1 Ex 5 and 6
|*EX 5*|*EX 6*|
|:-------------:|:-------------:|
|![Sq. Ft to Acres.](img/q5.png) | ![Miles Per Gal](img/q6.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mmm18m/stationlocation/src/master/ "Bitbucket Station Locations")