SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mmm18m')
DROP DATABASE mmm18m;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mmm18m')
CREATE DATABASE mmm18m;
GO

use mmm18m;
GO

-- --------------------------------------------------------------------
-- table creation
-----------------------------------------------------------------------

IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person 
(
	per_id SMALLINT not null identity(1,1),
	per_ssn binary(64) null,
	per_fname varchar(15) not null,
	per_lname varchar(30) not null,
	per_gender char(1) not null check (per_gender IN('f','m')),
	per_dob date not null,
	per_street varchar(30) not null,
	per_city varchar(30) not null,
	per_state char(2) not null default 'FL',
	per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	per_email varchar(100) null,
	per_type char(1) not null check (per_type in('c','s')),
	per_notes varchar(45) null,
	PRIMARY KEY (per_id),

	CONSTRAINT ux_per_ssn unique nonclustered (per_ssn asc)
	);


IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone 
(
	phn_id smallint not null identity(1,1),
	per_id smallint not null,
	phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	phn_type char(1) not null check (phn_type in('h','c','w','f')),
	phn_notes varchar(255) null,
	primary key (phn_id),

	constraint fk_phone_person
	foreign key (per_id)
	references dbo.person (per_id)
	on delete cascade
	on update cascade
);

IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
	per_id smallint not null,
	cus_balance decimal(7,2) not null check (cus_balance >= 0 ),
	cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
	cus_notes varchar(45) null,
	primary key (per_id),

	constraint fk_customer_person
	foreign key (per_id)
	references dbo.person (per_id)
	on delete cascade
	on update cascade
);

IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep 
(
	per_id smallint not null,
	srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
	srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
	srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
	srp_notes varchar (45) null,
	primary key (per_id),

	constraint fk_slsrep_person
	foreign key (per_id)
	references dbo.person (per_id)
	on delete cascade
	on update cascade
);

IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
	sht_id smallint not null identity(1,1),
	per_id smallint not null,
	sht_type char(1) not null check (sht_type in('i','u','d')),
	sht_modified datetime not null,
	sht_modifier varchar(45) not null default system_user,
	sht_date date not null default getDate(),
	sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
	sht_ytd_total_sales decimal(8,2) not null check (sht_ytd_total_sales >= 0),
	sht_ytd_total_comm decimal(7,2) not null check (sht_ytd_total_comm >= 0),
	sht_notes varchar (45) null,
	PRIMARY KEY (sht_id),

	constraint fk_srp_hist_slsrep
	foreign key (per_id)
	references dbo.slsrep (per_id)
	on delete cascade
	on update cascade

);

IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact 
(
	cnt_id int not null identity (1,1),
	per_cid smallint not null,
	per_sid smallint not null,
	cnt_date datetime not null,
	cnt_notes varchar(255) null,
	primary key (cnt_id),

	constraint fk_contact_customer
	foreign key (per_cid)
	references dbo.customer (per_id)
	on delete cascade
	on update cascade,

	constraint fk_contact_slsrep
	foreign key (per_sid)
	references dbo.slsrep (per_id)
	on delete no action
	on update no action
);


IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
	ord_id int not null identity(1,1),
	cnt_id int not null,
	ord_placed_date datetime not null,
	ord_filled_date datetime null,
	ord_notes varchar(255) null,
	primary key (ord_id),

	CONSTRAINT fk_order_contact
	FOREIGN KEY (cnt_id)
	REFERENCES dbo.contact (cnt_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
GO

CREATE TABLE dbo.region
(
reg_id tinyint not null identity(1,1),
reg_name char(1) not null,
reg_notes varchar(255) null,
primary key (reg_id)
);
GO

IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
GO

CREATE TABLE dbo.state
(
ste_id tinyint not null identity(1,1),
reg_id tinyint not null,
ste_name char(2) not null default 'FL',
ste_notes varchar(255) null,
primary key (ste_id),

CONSTRAINT fk_state_region
	FOREIGN KEY (reg_id)
	REFERENCES dbo.region (reg_id)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);
GO

IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
cty_id tinyint not null identity(1,1),
ste_id tinyint not null,
cty_name varchar(30) not null,
cty_notes varchar(255) null,
primary key (cty_id),

CONSTRAINT fk_city_state
	FOREIGN KEY (ste_id)
	REFERENCES dbo.state (ste_id)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);
GO

IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
	str_id smallint not null identity(1,1),
	cty_id tinyint not null,
	str_name varchar(45) not null,
	str_street varchar(30) not null,
	str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	str_email varchar(100) null,
	str_url varchar(100) not null,
	str_notes varchar(255) null,
	PRIMARY KEY (str_id),

	CONSTRAINT fk_store_city
		FOREIGN KEY (cty_id)
		REFERENCES dbo.city (cty_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
	inv_id int not null identity(1,1),
	ord_id int not null,
	str_id smallint not null,
	inv_date datetime not null,
	inv_total decimal(8,2) not null check (inv_total >= 0),
	inv_paid bit not null,
	inv_notes varchar(255) null,
	primary key (inv_id),

	CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

	CONSTRAINT fk_invoice_order
	foreign key (ord_id)
	references dbo.[order] (ord_id)
	on delete cascade
	on update cascade,

	CONSTRAINT fk_invoice_store
	foreign key (str_id)
	references dbo.store (str_id)
	on delete cascade
	on update cascade
);

IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
	pay_id int not null identity (1,1),
	inv_id int not null,
	pay_date DATETIME not null,
	pay_amt decimal(7,2) not null check (pay_amt >= 0),
	pay_notes varchar(255) null,
	primary key (pay_id),

	constraint fk_payment_invoice
	foreign key (inv_id)
	references dbo.invoice (inv_id)
	on delete cascade
	on update cascade
);

IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
	ven_id smallint not null identity(1,1),
	ven_name varchar(45) not null,
	ven_street varchar(30) not null,
	ven_city varchar(30) not null,
	ven_state char(2) not null default 'FL',
	ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	ven_email varchar(100) null,
	ven_url varchar(100) not null,
	ven_notes varchar(255) null,
	PRIMARY KEY (ven_id)
);

IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product 
(
	pro_id smallint not null identity (1,1),
	ven_id smallint not null,
	pro_name varchar(30) not null,
	pro_descript varchar(45) not null,
	pro_weight FLOAT NOT NULL check (pro_weight >= 0),
	pro_qoh smallint NOT NULL check (pro_qoh >= 0),
	pro_cost decimal(7,2) NOT NULL check (pro_cost >= 0),
	pro_price decimal(7,2) NOT NULL check (pro_price >= 0),
	pro_discount decimal(3,0) null,
	pro_notes varchar(255) null,
	primary key (pro_id),

	CONSTRAINT fk_product_vendor
	FOREIGN KEY (ven_id)
	REFERENCES dbo.vendor (ven_id)
	ON UPDATE CASCADE
	ON DELETE CASCADE

);

IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
	pht_id int not null identity(1,1),
	pro_id smallint not null,
	pht_date datetime not null,
	pht_cost decimal(7,2) not null check (pht_cost >= 0),
	pht_price decimal(7,2) not null check (pht_price >= 0),
	pht_discount decimal(3,0) null,
	pht_notes varchar(255) null,
	PRIMARY KEY (pht_id),

	CONSTRAINT fk_product_hist_product
	FOREIGN KEY (pro_id)
	REFERENCES dbo.product (pro_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.orderln', N'U') IS NOT NULL
DROP TABLE dbo.orderln;
GO

CREATE TABLE dbo.orderln 
(
	oln_id int not null identity(1,1),
	ord_id int not null,
	pro_id smallint not null,
	oln_qty smallint not null check (oln_qty >= 0),
	oln_price decimal(7,2) not null check (oln_price >= 0),
	oln_notes varchar(255) null,
	primary key (oln_id),

	CONSTRAINT fk_order_line_order
	FOREIGN KEY (ord_id)
	REFERENCES dbo.[order] (ord_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,

	CONSTRAINT fk_order_line_product
	FOREIGN KEY (pro_id)
	REFERENCES dbo.product (pro_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time
(
tim_id int not null identity(1,1),
tim_yr smallint not null,
tim_qtr tinyint not null,
tim_month tinyint not null,
tim_week tinyint not null,
tim_day tinyint not null,
tim_time time not null,
tim_notes varchar(255) null,
primary key (tim_id)
);
GO

IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale
(
	pro_id smallint not null,
	str_id smallint not null,
	cnt_id int not null,
	tim_id int not null,
	sal_qty smallint not null,
	sal_price decimal (8,2) not null,
	sal_total decimal (8,2) not null,
	sal_notes varchar(255) null,
	primary key (pro_id, cnt_id, tim_id, str_id),

	CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
	unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

	CONSTRAINT fk_sale_time
	FOREIGN KEY (tim_id)
	REFERENCES dbo.time (tim_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,

	CONSTRAINT fk_sale_contact
	FOREIGN KEY (cnt_id)
	REFERENCES dbo.contact (cnt_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,

	CONSTRAINT fk_sale_store
	FOREIGN KEY (str_id)
	REFERENCES dbo.store (str_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,

	CONSTRAINT fk_sale_product
	FOREIGN KEY (pro_id)
	REFERENCES dbo.product (pro_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE

);


SELECT * FROM Information_schema.tables;

SELECT HASHBYTES('SHA2_512', 'test');

SELECT len(HASHBYTES('SHA2_512', 'test'));

INSERT INTO dbo.region (reg_name, reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);
GO

INSERT INTO dbo.state (reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'IL', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(3, 'TX', NULL);
GO

INSERT INTO dbo.city (ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(3, 'Aspen', NULL),
(4, 'Chicago', NULL),
(5, 'Clover', NULL),
(3, 'St. Louis', NULL);
GO


INSERT INTO dbo.store (cty_id, str_name, str_street, str_zip, str_phone, str_email, str_url, str_notes) 
VALUES 
(2, 'Walgreens', '14567 Walnut Ln',  '475315690', '3127658127', 'info@walgreens.com', 'http://www.walgreens.com', NULL), 
(3, 'CVS', '572 Casper Rd', '505231519', '3128926534', 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger.'), 
(4, 'Lowes', '81309 Catapult Ave', '802345671', '9017653421', 'sales@lowes.com', 'http://www.lowes.com', NULL), 
(5, 'Walmart', '14567 Walnut Ln', '387563628', '8722718923', 'info@walmart.com','http://www.walmart.com', NULL), 
(1, 'Dollar General', '47583 Davison Rd', '482983456', '3137583492', 'ask@dollargeneral.com', 'http://www.dollargeneral.com', NULL);

INSERT INTO dbo.person (per_ssn, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes) 
VALUES 
(HASHBYTES('SHA2_512', 'test1'), 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL), 
(HASHBYTES('SHA2_512', 'test2'), 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 983208440, 'bwayne@knology.net', 's', NULL), 
(HASHBYTES('SHA2_512', 'test3'), 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', 's', NULL), 
(HASHBYTES('SHA2_512', 'test4'), 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 132084409, 'thompson@gmail.com', 's', NULL), 
(HASHBYTES('SHA2_512', 'test5'), 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL), 
(HASHBYTES('SHA2_512', 'test6'), 'Tony', 'Stark', 'm', '1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test7'), 'Hank', 'Pym', 'm', '1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(HASHBYTES('SHA2_512', 'test8'), 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(HASHBYTES('SHA2_512', 'test9'), 'Sandra', 'Dole', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com', 'c', NULL),
(HASHBYTES('SHA2_512', 'test10'), 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'S', 562638332, 'bavery@hotmail.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test11'), 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test12'), 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test13'), 'Adam', 'Jurris', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test14'), 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 672048823,'jsleen@symaptico.com', 'c', NULL), 
(HASHBYTES('SHA2_512', 'test15'), 'Bill', 'Neiderheim', 'm', '1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'breiderheim@comcast.net', 'c',NULL);

INSERT INTO dbo.slsrep (per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL), 
(2, 80000, 35000, 3500, NULL), 
(3, 150000, 84000, 9650, 'Great salesperson!'), 
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);

INSERT INTO dbo.customer (per_id, cus_balance, cus_total_sales, cus_notes) 
VALUES 
(6, 120, 14789, NULL), 
(7, 98.46, 234.92, NULL), 
(8,0, 4578, 'Customer always pays on time.'), 
(9, 981.73, 1672.38, 'High balance.'), 
(10, 541.23, 782.57, NULL), 
(11, 251.02, 13782.96, 'Good customer.'), 
(12, 582.67, 963.12, 'Previously paid in full.'), 
(13, 121.67, 1057.45, 'Recent customer.'), 
(14, 765.43, 6789.42, 'Buys bulk quantities.'), 
(15, 304.39, 456.81, 'Has not purchased recently.');


INSERT INTO dbo.vendor (ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes) 
VALUES
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL), 
('General Electric', '100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'Very good turnaround'), 
('Cisco', '300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL), 
('Goodyear', '100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone.'),
('Snap-On', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!');

INSERT INTO dbo.product (ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes) 
VALUES
(1,'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'), 
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, NULL, NULL), 
(3, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale.'),
(4, 'pail', '16 Gallon', 2.8, 48, 3.89, 7.99, 40, NULL), 
(5, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons');


INSERT INTO dbo.product_hist (pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes) 
VALUES 
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL), 
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL), 
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'), 
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price sale.');

INSERT INTO dbo.srp_hist (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_total_sales, sht_ytd_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL), 
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL), 
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);

INSERT INTO dbo.phone (per_id, phn_num, phn_type, phn_notes)
VALUES
(1, '1112223333', 'h', NULL),
(2, '2223334444', 'h', NULL),
(3, '3334445555', 'h', NULL),
(4, '4445556666', 'h', NULL),
(5, '5556667777', 'h', NULL);

INSERT INTO dbo.contact (per_sid, per_cid, cnt_date, cnt_notes) 
VALUES 
(1, 6, '1999-01-01', NULL), 
(2,6, '2001-09-29', NULL), 
(3, 7, '2002-08-15', NULL), 
(2,7, '2002-09-01', NULL), 
(4, 7, '2004-01-05', NULL), 
(5, 8, '2004-02-28', NULL), 
(4, 8, '2004-03-03', NULL), 
(1,9, '2004-04-07', NULL), 
(5,9, '2004-07-29', NULL), 
(3, 11, '2005-05-02', NULL), 
(4, 13, '2005-06-14', NULL), 
(2, 15, '2005-07-02', NULL);

INSERT INTO dbo.[order] (cnt_id, ord_placed_date, ord_filled_date, ord_notes) 
VALUES 
(1, '2010-11-23', '2010-12-24', NULL), 
(2, '2005-03-19', '2005-07-28', NULL), 
(3, '2011-07-01', '2011-07-06', NULL), 
(4, '2009-12-24', '2010-01-05', NULL), 
(5, '2008-09-21', '2008-11-26', NULL), 
(6, '2009-04-17', '2009-04-30', NULL), 
(7, '2010-05-31', '2010-06-07', NULL), 
(8, '2007-09-02', '2007-09-16', NULL), 
(9, '2011-12-08', '2011-12-23', NULL), 
(10, '2012-02-29', '2012-05-02', NULL);

INSERT INTO dbo.invoice (ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes) 
VALUES 
(5, 1, '2001-05-03', 58.32, 0, NULL), 
(4, 1, '2006-11-11', 100.59, 0, NULL), 
(1, 1, '2010-09-16', 57.34, 0, NULL), 
(3, 2, '2011-01-10', 99.32, 1, NULL), 
(2, 3, '2008-06-24', 1109.67, 1, NULL), 
(6, 4, '2009-04-20', 239.83, 0, NULL), 
(7, 5, '2010-06-05', 537.29, 0, NULL), 
(8, 2, '2007-09-09', 644.21, 1, NULL), 
(9, 3, '2011-12-17', 934.12, 1, NULL), 
(10, 4, '2012-03-18', 27.45, 0, NULL);

INSERT INTO dbo.orderln (ord_id, pro_id, oln_qty, oln_price, oln_notes) 
VALUES 
(1, 2, 10, 8.0, NULL), 
(2, 3, 7, 9.88, NULL), 
(3, 4, 3, 6.99, NULL), 
(5, 1, 2, 12.76, NULL), 
(4, 5, 13, 58.99, NULL);


INSERT INTO dbo.payment (inv_id, pay_date, pay_amt, pay_notes) 
VALUES 
(5, '2008-07-01', 5.99, NULL), 
(4, '2010-09-28', 4.99, NULL), 
(1, '2008-07-23', 8.75, NULL),
(3, '2010-10-31', 19.55, NULL), 
(2, '2011-03-29', 32.5, NULL), 
(6, '2010-10-03', 20.00, NULL), 
(8, '2008-08-09', 1000.00, NULL), 
(9, '2009-01-10', 103.68, NULL), 
(7, '2007-03-15', 25.00, NULL), 
(10, '2007-05-12', 40.00, NULL), 
(4, '2007-05-22', 9.33, NULL);




INSERT INTO dbo.time (tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL),
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL),
(2014, 1, 2, 8, 2, '12:27:14', NULL),
(2013, 3, 9, 38, 4, '10:12:28', NULL),
(2012, 4, 11, 47, 3, '22:36:22', NULL),
(2014, 2, 6, 23, 3, '19:07:10', NULL);
GO

INSERT INTO sale
(pro_id, str_id, cnt_id, tim_Id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 5, 5, 3, 20, 9.99, 199.8, NULL),
(2, 4, 6, 2, 5, 5.99, 29.95, NULL),
(3, 3, 4, 1, 30, 3.99, 119.7, NULL),
(4, 2, 1, 5, 15, 18.99, 284.85, NULL),
(5, 1, 2, 4, 6, 11.99, 71.94, NULL),
(5, 2, 5, 6, 10, 9.99, 199.8, NULL),
(4, 3, 6, 7, 5, 5.99, 29.95, NULL),
(3, 1, 4, 8, 30, 3.99, 119.7, NULL),
(2, 3, 1, 9, 15, 18.99, 284.85, NULL),
(1, 4, 2, 10, 6, 11.99, 71.94, NULL),
(1, 2, 3, 11, 10, 11.99, 119.9, NULL);

Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (4, 2, 3, 2, 325, 19432.26, 48711.47, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (1, 5, 4, 1, 391, 94971.86, 2521.77, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (1, 4, 4, 4, 183, 59198.47, 91528.53, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (3, 2, 3, 4, 396, 78246.21, 92411.42, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (1, 5, 5, 1, 174, 89805.67, 49340.46, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (2, 2, 3, 2, 195, 27151.16, 8641.40, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (1, 1, 2, 1, 674, 45993.08, 94317.00, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (1, 2, 4, 5, 979, 27189.51, 75738.43, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (4, 2, 5, 2, 713, 22004.62, 37880.77, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (2, 4, 4, 5, 671, 50079.13, 43296.28, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (2, 4, 2, 4, 15, 99925.21, 25463.34, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (3, 1, 5, 4, 575, 38039.26, 72843.83, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (2, 2, 1, 5, 415, 30264.55, 10984.58, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (5, 1, 4, 4, 32, 52035.30, 61680.34, NULL);
Insert into dbo.sale (pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes) values (4, 3, 3, 2, 723, 14903.73, 63173.81, NULL);

GO
