# LIS3781
## Mary Meberg

### **Project 1 Requirements:**

*Four Parts*

1. Screenshot of 'person' table
2. Screenshot of tables in Oracle. 
3. The following tables should be created and populated with at least 5 records to the CCI server. 


#### Assignment 1 Screenshots:

#### Screenshot of ERD and Populated 'person' Table
|*ERD*|*'person' Table*|
|:-------------:|:-------------:|
|![IDLE Tip Calc.](img/erd.png) |![IDLE Tip Calc.](img/select_person.png)|

[Link to MWB File](https://bitbucket.org/mmm18m/lis-3781/src/master/p1/p1.mwb)

