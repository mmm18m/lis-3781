# LIS3784 - ADV DATABASE MANAGEMENT

## Mary Marguerite Meberg

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/a1/)
	* Installations
* A1
* Questions
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/a2/)
	* SQL Code + Screenshots
* Populate SQL tables using Windows CMD on local and remote servers
	* Questions

3. [A3 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/a3/)
    * Code on Oracle and send to server
* Screenshot code and tables
	* Questions

4. [A4 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/a4/)
	* SQL File
* ERD
	* Questions

  
5. [A5 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/a5/)
	* SQL File
* ERD
	* Questions

 
6. [P1 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/p1/)
	* Submit MWB File of ERD
* Submit screenshot of ERD.
* Submit screenshot of 'person' table.
	* Questions


7. [P2 README.md](https://bitbucket.org/mmm18m/lis-3781/src/master/p2/)
	* Submit screenshots of Mongo DB running
* Submit screenshots of Mongo DB execution statement
	* My last set of questions ever!