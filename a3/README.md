# LIS3781
## Mary Meberg

### **Assignment 3 Requirements:**

*Four Parts*

1. Screenshot of table creation and insert statements. 
2. Screenshot of tables in Oracle. 
3. The following tables should be created and populated with at least 5 records to the CCI server. 


#### Assignment Screenshots:

#### Code in Oracle- Creation of tables and insert statements
|*Screenshot 1*|*Screenshot 2*|
|:-------------:|:-------------:|
|![IDLE Tip Calc.](img/oracle_code_screenshot_1.png)| ![IDLE Tip Calc.](img/oracle_code_screenshot_2.png)|

|*Table 1 (Customer)*|*Table 2 (Commodity)*|*Table 3 (Order)*|
|:-------------:|:-------------:|:-------------:|
|![IDLE Tip Calc.](img/table_1.png)| ![IDLE Tip Calc.](img/table_2.png)| ![IDLE Tip Calc.](img/table_3.png)|


